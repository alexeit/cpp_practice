#include "rectangle.hpp"
#include <iostream>
#include <stdexcept>
#include <cmath>

drozdov::Rectangle::Rectangle(double width, double height, const point_t &pos):
  width_(width),
  height_(height),
  angle_(0.0),
  pos_(pos)
{
  if ((width <= 0.0) || (height <= 0.0)) {
    throw std::invalid_argument("Rectangle arguments are not valid.");
  }
}

void drozdov::Rectangle::rotate(double rotationAngle)
{
  angle_ = fmod(rotationAngle + angle_, 360.0);
}

void drozdov::Rectangle::printData() const
{
  std::cout << "\nRectangle";
  std::cout << "\nWidth: " << width_;
  std::cout << "\nHeight: " << height_;
  std::cout << "\nPosition: (" << pos_.x << ';' << pos_.y << ')';
}

double drozdov::Rectangle::getArea() const
{
  return (width_ * height_);
}

drozdov::point_t drozdov::Rectangle::getPos() const
{
  return pos_;
}

drozdov::rectangle_t drozdov::Rectangle::getFrameRect() const
{
  return {width_, height_, pos_};
}

void drozdov::Rectangle::scale(double coefficient)
{
  if (coefficient <= 0.0) {
    throw std::invalid_argument("\nRectangle's coefficient for scale is not valid.");
  }
  width_ *= coefficient;
  height_ *= coefficient;
}

void drozdov::Rectangle::move(double dx, double dy)
{
  pos_.x += dx;
  pos_.y += dy;
}

void drozdov::Rectangle::move(const point_t &newPos)
{
  pos_ = newPos;
}
