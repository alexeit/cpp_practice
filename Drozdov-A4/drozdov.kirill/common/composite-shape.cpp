#define _USE_MATH_DEFINES
#include "composite-shape.hpp"
#include <iostream>
#include <stdexcept>
#include <algorithm>
#include <cmath>

drozdov::CompositeShape::CompositeShape():
  qty_(0),
  shapes_(nullptr)
{ }

drozdov::CompositeShape::CompositeShape(const CompositeShape &other):
  qty_(other.qty_),
  shapes_(new Shape*[qty_])
{
  for (int index = 0; index < qty_; index++)
  {
    shapes_[index] = other.shapes_[index];
  }
}

drozdov::CompositeShape::CompositeShape(CompositeShape &&other):
  qty_(other.qty_),
  shapes_(other.shapes_)
{
  other.shapes_ = nullptr;
  other.qty_ = 0;
}

drozdov::CompositeShape::CompositeShape(Shape &startShape):
  qty_(1),
  shapes_(new Shape*[1])
{
  shapes_[0] = &startShape;
}

drozdov::CompositeShape::~CompositeShape()
{
  if (shapes_ != nullptr) {
    delete [] shapes_;
  }
}

void drozdov::CompositeShape::operator +=(Shape &newShape)
{
  addShape(newShape);
}

drozdov::CompositeShape& drozdov::CompositeShape::operator +(Shape &newShape)
{
  addShape(newShape);
  return *this;
}

drozdov::Shape* drozdov::CompositeShape::operator [](int index) const
{
  if (qty_ == 0) {
    throw std::logic_error("Composite-shape is empty for operator [].");
  } else if (index <= 0 || index > qty_) {
    throw std::out_of_range("Index was out of range in operator[].");
  }
  return shapes_[index - 1];
}

drozdov::CompositeShape& drozdov::CompositeShape::operator =(const CompositeShape &other)
{
  if (&other != this) {
    if (shapes_ != nullptr) {
      delete[] shapes_;
    }
    qty_ = other.qty_;
    shapes_ = new Shape*[qty_];
    for (int index = 0; index < qty_; index++)
    {
      shapes_[index] = other.shapes_[index];
    }
  }
  return *this;
}

drozdov::CompositeShape& drozdov::CompositeShape::operator =(CompositeShape &&other)
{
  if (&other != this) {
    if (shapes_ != nullptr) {
      delete[] shapes_;
    }
    qty_ = other.qty_;
    shapes_ = other.shapes_;
    other.qty_ = 0;
    other.shapes_ = nullptr;
  }
  return *this;
}

void drozdov::CompositeShape::addShape(Shape &newShape)
{
  if (exists(&newShape)) {
    throw std::logic_error("Two pointers to the object will spoil the work.");
  }

  if (qty_ == 0) {
    shapes_ = new Shape*[1];
    shapes_[qty_++] = &newShape;
  } else {
    drozdov::Shape **tempArray = new Shape*[qty_ + 1];
    for (int index = 0; index < qty_; index++)
    {
      tempArray[index] = shapes_[index];
    }
    tempArray[qty_++] = &newShape;
    delete [] shapes_;
    shapes_ = tempArray;
  }
}

void drozdov::CompositeShape::removeShape(int indexRemoveShape)
{
  if (indexRemoveShape <= 0 || indexRemoveShape > qty_) {
    throw std::out_of_range("There is no such object.");
  }

  if (qty_ == 1) {
    delete [] shapes_;
    shapes_ = nullptr;
  } else {
    drozdov::Shape **tempArray = new Shape *[qty_ - 1];
    for (int index = 0; index < qty_; index++)
    {
      if (index < indexRemoveShape - 1) {
        tempArray[index] = shapes_[index];
      } else if (index > indexRemoveShape - 1) {
        tempArray[index - 1] = shapes_[index];
      }
    }
    delete [] shapes_;
    shapes_ = tempArray;
  }
  --qty_;
}

int drozdov::CompositeShape::getQuantity() const
{
  return qty_;
}

void drozdov::CompositeShape::rotate(double rotationAngle)
{
  double cosAngle = cos(rotationAngle * M_PI / 180);
  double sinAngle = sin(rotationAngle * M_PI / 180);

  const drozdov::point_t posComp = getPos();

  for (int index = 0; index < qty_; index++)
  {
    const drozdov::point_t posShape = shapes_[index]->getPos();

    shapes_[index]->move({posComp.x + cosAngle * (posShape.x - posComp.x) - sinAngle * (posShape.y - posComp.y),
              posComp.y + cosAngle * (posShape.y - posComp.y) + sinAngle * (posShape.x - posComp.x)});
    shapes_[index]->rotate(rotationAngle);
  }
}

void drozdov::CompositeShape::printData() const
{
  if (qty_ != 0) {
    const drozdov::rectangle_t frameRect = getFrameRect();
    std::cout << "\nPosition: (" << frameRect.pos.x << ';' << frameRect.pos.y << ')';
    std::cout << "\nAmount of shapes: " << qty_;
    std::cout << "\nContent:";
    for (int index = 0; index < qty_; index++)
    {
      std::cout << "\n* * * * *";
      std::cout << "\nShape - №" << index + 1;
      shapes_[index]->printData();
    }
    std::cout << "\n* * * * *";
  } else {
    std::cout << "\nComposite-Shape is empty.";
  }
}

double drozdov::CompositeShape::getArea() const
{
  if (qty_ == 0) {
    throw std::logic_error("\nComposite-Shape is empty for get area.");
  }
  double area = 0.0;
  for (int index = 0; index < qty_; index++)
  {
    area += shapes_[index]->getArea();
  }
  return area;
}

drozdov::point_t drozdov::CompositeShape::getPos() const
{
  if (qty_ == 0) {
    throw std::logic_error("\nComposite-Shape is empty for get position.");
  }
  return getFrameRect().pos;
}

drozdov::rectangle_t drozdov::CompositeShape::getFrameRect() const
{
  if (qty_ == 0) {
    throw std::logic_error("\nComposite-Shape is empty for  get frame rectangle.");
  }

  drozdov::rectangle_t frameShape = shapes_[0]->getFrameRect();

  drozdov::point_t min = {frameShape.pos.x - frameShape.width / 2, frameShape.pos.y - frameShape.height / 2};
  drozdov::point_t max = {frameShape.pos.x + frameShape.width / 2, frameShape.pos.y + frameShape.height / 2};

  for (int index = 1; index < qty_; index++)
  {
    frameShape = shapes_[index]->getFrameRect();
    min.x = std::min((frameShape.pos.x - frameShape.width / 2), min.x);
    min.y = std::min((frameShape.pos.y - frameShape.height / 2), min.y);

    max.x = std::max((frameShape.pos.x + frameShape.width / 2), max.x);
    max.y = std::max((frameShape.pos.y + frameShape.height / 2), max.y);
  }
  const double width = max.x - min.x;
  const double height = max.y - min.y;

  return {width, height, {min.x + width / 2, min.y + height / 2}};
}

void drozdov::CompositeShape::scale(double coefficient)
{
  if (coefficient <= 0.0) {
    throw std::invalid_argument("\nComposite-Shape's coefficient for  scale is not valid.");
  } else if (qty_ == 0) {
    throw std::logic_error("\nComposite-Shape is empty for scale.");
  }

  const drozdov::point_t posComposite = getFrameRect().pos;
  for (int index = 0; index < qty_; index++)
  {
    const drozdov::point_t posShape = shapes_[index]->getPos();
    const drozdov::point_t distance = {posComposite.x + coefficient * (posShape.x - posComposite.x),
            posComposite.y + coefficient * (posShape.y - posShape.y)};
    shapes_[index]->move(distance);
    shapes_[index]->scale(coefficient);
  }
}

void drozdov::CompositeShape::move(double dx, double dy)
{
  if (qty_ == 0) {
    throw std::logic_error("\nComposite-Shape is empty for move.");
  }

  for (int index = 0; index < qty_; index++)
  {
    shapes_[index]->move(dx,dy);
  }
}

void drozdov::CompositeShape::move(const point_t &newPos)
{
  if (qty_ == 0) {
    throw std::logic_error("\nComposite-Shape is empty for move.");
  }
  const drozdov::point_t posComposite = getFrameRect().pos;
  const drozdov::point_t difference = {newPos.x - posComposite.x, newPos.y - posComposite.y};
  move(difference.x, difference.y);
}

bool drozdov::CompositeShape::exists(const Shape *shape) const
{
  bool answer = false;
  for (int index = 0; index < qty_; index++)
  {
    if (shapes_[index] == shape) {
      answer = true;
      break;
    }
  }
  return answer;
}
