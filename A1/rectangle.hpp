#ifndef RECTANGLE_HPP
#define RECTANGLE_HPP

#include "shape.hpp"

class Rectangle : public Shape
{
public:	
	Rectangle(const double width, const double height, const point_t & pos); // для pos используем ссылку, так как это - структура
	virtual ~Rectangle() { std::cout << __FUNCTION__ << std::endl; }
	virtual double getArea() const;
	virtual rectangle_t getFrameRect() const;
	virtual void move(const point_t & pos); // смещение в заданную точку
	virtual void move(double x, double y); // смещение по осям

private:
	rectangle_t rect_;		
};

#endif // RECTANGLE_HPP