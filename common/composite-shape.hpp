#ifndef COMPOSITE_SHAPE_HPP
#define COMPOSITE_SHAPE_HPP

#include "shape.hpp"
#include <cstddef>

namespace tchervinsky
{
  class CompositeShape : public Shape
  {
  public:
    CompositeShape();
    CompositeShape(const CompositeShape & other);
    CompositeShape(CompositeShape && other);
    ~CompositeShape();
    CompositeShape & operator =(const CompositeShape & other);
    CompositeShape & operator =(CompositeShape && other);
    double getArea() const override;
    rectangle_t getFrameRect() const override;
    void move(const point_t & pos) override;
    void move(double dx, double dy) override;
    void scale(double coefficient) override;
    void addShape(Shape * shape);
    void removeShape(size_t index);

  private:
    std::size_t count_;
    Shape ** shapes_;
    point_t center_;
  };
}

#endif
