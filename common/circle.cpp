#include "circle.hpp"

#include <iostream>
#include <cmath>


// https://stackoverflow.com/questions/22502952/m-pi-issue-in-visual-studio
#ifndef M_PI  // for Windows etc
namespace
{
const double M_PI = std::acos(-1.0);
}
#endif

namespace tchervinsky {

Circle::Circle(double radius, const point_t & pos):
  radius_(radius),
  pos_(pos)
{
  if (radius <= 0.0) {
    throw std::invalid_argument("Invalid circle radius, shall be greater than 0.0");
  }
}

double Circle::getArea() const
{
  return (M_PI * radius_ * radius_);
}

rectangle_t Circle::getFrameRect() const
{
  // Return Value Optimization
  return rectangle_t { 2 * radius_, 2 * radius_, pos_ };
}

void Circle::move(const point_t & pos) // смещение в заданную точку
{
  pos_.x = pos.x;
  pos_.y = pos.y;
}

void Circle::move(double x, double y) // смещение по осям
{
  pos_.x += x;
  pos_.y += y;
}

void Circle::scale(const double coefficient) // масштабирование
{
  if (coefficient <= 0.0) {
    throw std::invalid_argument("Invalid circle scale coefficient, shall be greater than 0.0");
  }
  radius_ *= coefficient;
}

} // namespace tchervinsky
