#ifndef POLYGON_HPP
#define POLYGON_HPP

#define POINTS_REFS
//#undef POINTS_REFS

#include "shape.hpp"

#include <cstddef>  // std::size_t

namespace tchervinsky
{
  class Polygon : public Shape
  {
    public:
      Polygon(); // default constructor
#ifdef POINTS_REFS
      Polygon(const std::size_t numPoints, point_t **polyPoints);
#else
      Polygon(const std::size_t numPoints, const point_t *polyPoints);
#endif
      /*
        https://cpppatterns.com/patterns/rule-of-five.html
        https://en.cppreference.com/w/cpp/language/rule_of_three
      */
      Polygon(const Polygon & other);
      Polygon(Polygon && other);
      ~Polygon();
      Polygon & operator =(const Polygon & other);
      Polygon & operator =(Polygon && other);

      double getArea() const override;
      rectangle_t getFrameRect() const override;
      void move(const point_t & pos) override; // смещение в заданную точку
      void move(double x, double y) override; // смещение по осям
      void scale(const double coefficient) override; // масштабирование
      point_t calcCenroid() const; // цетнр
      bool isConvex() const; // выпуклость
      void printInfo();
    private:
      std::size_t numPoints_;
      point_t center_;
#ifdef POINTS_REFS
      point_t **points_;
#else
      point_t *points_;
#endif
  };

}
#endif
