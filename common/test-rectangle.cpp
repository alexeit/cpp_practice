#include <stdexcept>
#include <boost/test/auto_unit_test.hpp>

#include "rectangle.hpp"

const double TOLERANCE = 0.01; // tolerance in percents

BOOST_AUTO_TEST_SUITE(test_A2)

BOOST_AUTO_TEST_CASE(Test_rectangle_area) {
  tchervinsky::Rectangle rect (10.0, 5.0, {15.0, 15.0});
  BOOST_REQUIRE_CLOSE(rect.getArea(), 50.0, TOLERANCE);
}

BOOST_AUTO_TEST_CASE(Test_rectangle_constructor_fail_throw)
{
  BOOST_CHECK_THROW(tchervinsky::Rectangle rect(-10.0, 5.0, {15.0, 15.0}), std::invalid_argument);
}

BOOST_AUTO_TEST_CASE(Test_scale_negativer_fail_throw)
{
  tchervinsky::Rectangle rect(10.0, 5.0, {15.0, 15.0});
  BOOST_CHECK_THROW(rect.scale(-1.0), std::invalid_argument);
}

BOOST_AUTO_TEST_SUITE_END()
