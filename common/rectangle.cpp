#include "rectangle.hpp"

#include <iostream>
#include <stdexcept>

namespace tchervinsky {

  Rectangle::Rectangle(double width, double height, const point_t & pos):
    rect_ (rectangle_t {width, height, pos})
  {
      if (width <= 0.0) {
        throw std::invalid_argument("Invalid rectangle width, shall be greater than 0.0");
      }
      if (height <= 0.0) {
        throw std::invalid_argument("Invalid rectangle height, shall be greater than 0.0");
      }
  }

  double Rectangle::getArea() const
  {
    return (rect_.width * rect_.height);
  }

  rectangle_t Rectangle::getFrameRect() const
  {
    return rect_;
  }

  void Rectangle::move(const point_t & pos) // смещение в заданную точку
  {
    rect_.pos.x = pos.x;
    rect_.pos.y = pos.y;
  }

  void Rectangle::move(double x, double y) // смещение по осям
  {
    rect_.pos.x += x;
    rect_.pos.y += y;
  }

  void Rectangle::scale(const double coefficient) // масштабирование
  {
    if (coefficient <= 0.0) {
      throw std::invalid_argument("Invalid rectangle scale coefficient, shall be greater than 0.0");
    }
    rect_.width *= coefficient;
    rect_.height *= coefficient;
  }

} // namespace tchervinsky
