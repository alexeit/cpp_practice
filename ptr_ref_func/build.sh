#!/bin/bash

# exit by error
set -e

echo Компилируем
g++ -o main -std=c++14 -Wall -Wextra -Wold-style-cast main.cpp 

echo При успешной компиляции, запускаем
./main