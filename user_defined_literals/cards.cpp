// Пример литералов. определяемых пользователем (лекция 4 - Функции. слайды 20-24)

#include <iostream>
#include <locale>

// Рассмотрим на примере карточной игры из книги Лафоре (Глава 4, стр. 154)


namespace card_suit { // используем пространство имен для карточной масти

// Используем структуры как классы - в будущем сможем использовать шаблоны

// трефы
	struct CardSuitClub {
		CardSuitClub(const wchar_t* value_w)
	 	{
	 		value_w_ = value_w;
	 		suit_w_= L"\u2667";
	 	}

		friend std::wostream& operator << (std::wostream & out, const CardSuitClub & suit) {
			out << (suit.value_w_)  << suit.suit_w_;
			return out;
		}

			const wchar_t* value_w_;
			const wchar_t* suit_w_;	
	};


// бубны
	struct CardSuitDia	 {
		CardSuitDia(const wchar_t* value_w)
	 	{
	 		value_w_ = value_w;
	 		suit_w_= L"\u2662";
	 	}

		friend std::wostream& operator << (std::wostream & out, const CardSuitDia & suit) {
			out << (suit.value_w_)  << suit.suit_w_;
			return out;
		}

			const wchar_t* value_w_;
			const wchar_t* suit_w_;	
	};

// черви
	struct CardSuitHeart	 {
		CardSuitHeart(const wchar_t* value_w)
	 	{
	 		value_w_ = value_w;
	 		suit_w_= L"\u2661";
	 	}

		friend std::wostream& operator << (std::wostream & out, const CardSuitHeart & suit) {
			out << (suit.value_w_)  << suit.suit_w_;
			return out;
		}

			const wchar_t* value_w_;
			const wchar_t* suit_w_;	
	};

// пики
	struct CardSuitSpade	 {
		CardSuitSpade(const wchar_t* value_w)
	 	{
	 		value_w_ = value_w;
	 		suit_w_= L"\u2664";
	 	}

		friend std::wostream& operator << (std::wostream & out, const CardSuitSpade & suit) {
			out << (suit.value_w_)  << suit.suit_w_;
			return out;
		}

			const wchar_t* value_w_;
			const wchar_t* suit_w_;	
	};

	// Определяем литералы
		CardSuitClub operator "" _club(const wchar_t *val, size_t ) {
		return CardSuitClub(val);
	}

		CardSuitDia operator "" _dia(const wchar_t *val, size_t ) {
		return CardSuitDia(val);
	}

		CardSuitHeart operator "" _heart(const wchar_t *val, size_t ) {
		return CardSuitHeart(val);
	}

		CardSuitSpade operator "" _spade(const wchar_t *val, size_t ) {
		return CardSuitSpade(val);
	}
} // namespace


using namespace card_suit;

int main()
{

	// Используем wide вывод для возможности вывода 32-битных символов Unicode 

	std::wcout.imbue(std::locale("")); // устанавливаем локаль по умолчанию

	std::wcout << L"2"_club << std::endl;

	std::wcout << L"5"_dia << std::endl;

	std::wcout << L"Валет"_heart << std::endl;

	std::wcout << L"Дама"_spade << std::endl;

	std::wcout << L"Можем использовать литералы в переменных" << std::endl; // обязательно wcout во всем коде

// Можем использовать литералы в переменных
	CardSuitClub tuz_tref = L"Туз"_club;
	std::wcout << tuz_tref << std::endl;

	CardSuitClub *tuz_tref_ref = &tuz_tref; 
	std::wcout << *tuz_tref_ref << std::endl;

	return 0;
}